package com.sda.dropbox.mail;

import com.sda.dropbox.config.ConfigService;
import com.sda.dropbox.mail.client.MailJetClient;
import com.sda.dropbox.mail.client.SendGridClient;

import static com.sda.dropbox.config.Keys.EMAIL_CLIENT;

public class ClientProvider {
    public static final String MAILJET="mailjet";
    public static final String SENDGRID = "sendgrid";

    public static EmailClient create(ConfigService cfg){
        String client = cfg.get(EMAIL_CLIENT);
        if (client.equals(MAILJET))
            return new MailJetClient();
        else if (client.equals(SENDGRID))
            return new SendGridClient();
        else throw new RuntimeException("No such client available");
    }
}
