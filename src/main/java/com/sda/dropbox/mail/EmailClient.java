package com.sda.dropbox.mail;

public interface EmailClient {
    void send(Email e);
}
