package com.sda.dropbox.listener;

import com.sda.dropbox.config.ConfigService;
import com.sda.dropbox.upload.DropBoxUploader;
import com.sda.dropbox.upload.Uploader;

import java.io.IOException;
import java.nio.file.*;

import static com.sda.dropbox.config.Keys.DIRECTORY;

public class DirectoryListener {

    private final Uploader uploader;
    private final String dir;
   // private static final String DIR = "C:\\Kurs Java\\";

    public DirectoryListener(Uploader uploader, ConfigService cfg) {
        this.uploader = uploader;
        this.dir = cfg.get(DIRECTORY);
    }

    public void listen() {
        try {
            WatchService watchService
                    = FileSystems.getDefault().newWatchService();

            Path path = Paths.get(dir);

            path.register(
                    watchService,
                    StandardWatchEventKinds.ENTRY_CREATE);

            WatchKey key;
            while ((key = watchService.take()) != null) {
                String name = (String) key.pollEvents().get(0).context().toString();
                System.out.println(name);
//            while(!sourceFile.renameTo(sourceFile)) {
                // Cannot read from file, windows still working on it.
//                Thread.sleep(10);
//            }
                uploader.upload(dir + name, name);
                key.reset();
            }
        } catch (IOException | InterruptedException e) {
            throw new ListenerException("Can not listen to directory" +dir, e);
        }

    }
}
