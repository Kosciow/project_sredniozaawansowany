package com.sda.dropbox;

import com.sda.dropbox.config.ConfigService;
import com.sda.dropbox.listener.DirectoryListener;
import com.sda.dropbox.mail.ClientProvider;
import com.sda.dropbox.mail.EmailClient;
import com.sda.dropbox.upload.DropBoxUploader;
import com.sda.dropbox.upload.Uploader;

import java.io.IOException;

public class Main {

    private static final int PROPS_INDEX=0;
    public static void main(String[] args) throws IOException {
        String propsPath = args[PROPS_INDEX];
        ConfigService cfg = new ConfigService(propsPath).load();


        EmailClient emailClient = ClientProvider.create(cfg);
        Uploader u =new DropBoxUploader(cfg,emailClient);
        DirectoryListener listener =new DirectoryListener(u,cfg);

        listener.listen();


    }
}
