package com.sda.dropbox.upload;

import com.dropbox.core.DbxException;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.v2.DbxClientV2;
import com.sda.dropbox.config.ConfigService;
import com.sda.dropbox.mail.Email;
import com.sda.dropbox.mail.EmailClient;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import static com.sda.dropbox.config.Keys.*;
import static java.lang.String.format;


public class DropBoxUploader implements Uploader{

    private final ConfigService cfg;
    private final EmailClient client;

    public DropBoxUploader(ConfigService cfg, EmailClient client) {
        this.cfg = cfg;
        this.client = client;
    }

    @Override
    public void upload(String path, String name){
 //       String ACCESS_TOKEN = "zT5d_XvuEEAAAAAAAAAAmZ9r3FRT0BUwDLfnSZATCObzawfpra4MO9h3fmSa-2KQ";
        String ACCESS_TOKEN = cfg.get(DROPBOX_KEY);
    DbxRequestConfig config = DbxRequestConfig.newBuilder("dropbox/java-tutorial").build();
    DbxClientV2 dbclient = new DbxClientV2(config, ACCESS_TOKEN);

        try (InputStream in = new FileInputStream(path)) {
        dbclient.files().uploadBuilder("/" +name)
                .uploadAndFinish(in);
        String content = format(cfg.get(EMAIL_CONTENT), name);
        client.send(new Email(cfg.get(EMAIL_SUBJECT), content, cfg.get(EMAIL_TO)));
    }
         catch (IOException | DbxException e) {

            throw new UploadException("Can not upload file"+ path, e);
        }
    }

}
